<?php
class Router{

	protected $uri;
	protected $controller;
	protected $action;
	protected $params;
	protected $route;
	protected $method_prefix;
	protected $language;

	/**
	* @return $this->uri
	*/
	public function getUri(){

		return $this->uri;
	}

	/**
	* @return $this->controller
	*/
	public function getController(){

		return $this->controller;
	}

	/**
	* @return $this->action;
	*/
	public function getAction(){

		return $this->action;
	}

	/**
	* @return $this->params
	*/
	public function getParams(){

		return $this->params;
	}

	/**
	* @return $this->route
	*/
	public function getRoute(){

		return $this->route;
	}

	/**
	* @return $this->method_prefix
	*/
	public function getMethodPrefix(){

		return $this->method_prefix;
	}

	/**
	* @return $this->language
	*/
	public function getLanguage(){

		return $this->language;
	}

	public function __construct($uri){

		$this->uri = urldecode(trim($uri, '/'));

		//die($this->uri);

		//Get defaults
		$routes = Config::get('routes');
		$this->route = Config::get('default_route');
		$this->method_prefix = isset($routes[$this->route]) ? $routes[$this->route] : '';
		$this->language = Config::get('default_language');
		$this->controller = Config::get('default_controller');
		$this->action = Config::get('default_action');

		$uri_parts = explode('?', $this->uri);

		//Get path like /lng/controller/action/param1/param2/.../...
		$path = $uri_parts[0];

		$Path_parts = explode('/', $path);


		if(count($Path_parts)){

            array_shift($Path_parts);
			//die(current($Path_parts));

			//Get the route or language at first element
			if(in_array(strtolower(current($Path_parts)), array_keys($routes))){

				$this->route = strtolower(current($Path_parts));
				$this->method_prefix = isset($routes[$this->route]) ? $routes[$this->route] : '';
				array_shift($Path_parts);
			} elseif (in_array(strtolower(current($Path_parts)), Config::get('languages'))) {
				
				$this->language = strtolower(current($Path_parts));
				array_shift($Path_parts);
			}

			// Get controller next element of array
			if(current($Path_parts)){

				//die(current($Path_parts));
				$this->controller = strtolower(current($Path_parts));
				array_shift($Path_parts);
			}

			// Get action
			if(current($Path_parts)){

				$this->action = strtolower(current($Path_parts));
				array_shift($Path_parts);
			}

			// Get params - all the rest
			$this->params = $Path_parts;
		}
	}



}